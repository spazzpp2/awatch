# awatch

Analytische Betrachtungen des Bundestags.

Daten via [Abgeordnetenwatch.de API](https://www.abgeordnetenwatch.de/api).

* [Bundestag 2009-2013](#bundestag-2009-2013)
* [Bundestag 2013-2017](#bundestag-2013-2017)
* [Bundestag 2017-2021](#bundestag-2017-2021)
* [Bundestag 2021-2025](#bundestag-2021-2025)

## Bundestag 2009-2013

![](img/blau-pro--rot-contra-83.jpg)

## Bundestag 2013-2017

![](img/blau-pro--rot-contra-97.jpg)

## Bundestag 2017-2021

![](img/blau-pro--rot-contra-111.jpg)

## Bundestag 2021-2025

![](img/blau-pro--rot-contra-132.jpg)
